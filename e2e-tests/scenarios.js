'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */
describe('scrumRetroHelper', function () {


    it('should automatically redirect to /scrumRetroTable when location hash/fragment is empty', function () {
        browser.get('index.html');
        expect(browser.getCurrentUrl()).toMatch("/scrumRetroTable");
    });


    describe('scrumRetroTable', function () {

        beforeEach(function () {
            browser.get('index.html#!/scrumRetroTable');

        });
        it('check if categories are complete', function () {

            //check if categories are complete
            var categories = $$(".category_title");
            expect(categories.count()).toEqual(5);
            expect(categories.getText()).toContain("Set the stage");
            expect(categories.getText()).toContain("Gather data");
            expect(categories.getText()).toContain("Generate insights");
            expect(categories.getText()).toContain("Decide what to do");
            expect(categories.getText()).toContain("Close the meeting");

        });
        it('check if data is corretly displayed on page', function () {
            var descriptions = $$(".activity_description");
            expect(descriptions.getText()).toContain("Brak elementów w bazie danych");
            expect(descriptions.get(1).getText()).toEqual("Test2");

        });
        it('check if main draw function work', function () {
            var descriptions = $$(".activity_description");
            var descBeforeNewDraw = descriptions.get(0).getText();
            var drawButton = element(by.id("drawAllButton"));
            browser.actions().mouseMove(drawButton).click().perform();
            descriptions = $$(".activity_description");
            var descAfterNewDraw = descriptions.get(0).getText();
            expect(descBeforeNewDraw).not.toEqual(descAfterNewDraw);

        });
        it('check if single draw function work', function () {
            var descriptions = $$(".activity_description");
            var descBeforeNewDraw = descriptions.get(0).getText();
            var firstArrow = $$(".arrows").first();
            descBeforeNewDraw = descriptions.get(0).getText();
            browser.actions().mouseMove(firstArrow).click().perform();
            descriptions = $$(".activity_description");
            var descAfterNewDraw = descriptions.get(0).getText();
            expect(descBeforeNewDraw).not.toEqual(descAfterNewDraw);

        });
        it('check if img show and hide correctly', function () {
            var EC = protractor.ExpectedConditions;
            var imgLink = $$(".img_click_link").first();
            browser.actions().mouseMove(imgLink).click().perform();
            browser.wait(EC.presenceOf($('.lightbox-image-container')), 500);
            browser.actions().mouseMove({x: 100, y: 100}).click().perform();
            browser.wait(EC.stalenessOf($('.lightbox-image-container')), 500);
            browser.actions().mouseMove(imgLink).click().perform();
            browser.wait(EC.presenceOf($('.lightbox-image-container')), 500);
            browser.actions().mouseMove($$('.close').first()).click().perform();
            browser.wait(EC.stalenessOf($('.lightbox-image-container')), 500);

        });

        it('check if possible to move to category search', function () {
            var EC = protractor.ExpectedConditions;
            var categoryLink = $$(".category_title").first();
            browser.actions().mouseMove(categoryLink).click().perform();
            browser.wait(EC.urlContains('scrumCategorySearch'), 500);


        });
        it('check if possible to move to category search', function () {
            var EC = protractor.ExpectedConditions;
            var categoryLink = $$(".category_title").first();
            browser.actions().mouseMove(categoryLink).click().perform();
            browser.wait(EC.urlContains('scrumCategorySearch'), 500);


        });
        it('check if possible to move to search', function () {
            var EC = protractor.ExpectedConditions;
            var searchLink = element(by.id("scrumSearchLink"));
            browser.actions().mouseMove(searchLink).click().perform();
            browser.wait(EC.urlContains('scrumSearch'), 500);


        });

    });

    describe('scrumCategorySearch', function () {
        beforeEach(function () {
            browser.get('index.html#!/scrumCategorySearch/Set%20the%20stage');
        });
        it("check if position numbers is 2 and have correct items", function () {
            var descriptions = $$(".activity_description");
            expect(descriptions.count()).toEqual(2);
            expect(descriptions.getText()).toContain("Test");
            expect(descriptions.getText()).toContain("Test secundo");
        });

        it("check if delete function show popup and close properly", function () {
            var EC = protractor.ExpectedConditions;
            var deleteButton =$$(".delete_button").first();
            browser.actions().mouseMove(deleteButton).click().perform();
            browser.wait(EC.presenceOf($('.md-dialog-content')), 500);
            browser.actions().mouseMove($$(".md-cancel-button").first()).click().perform();
            browser.wait(EC.stalenessOf($('.md-dialog-content')), 500);
            browser.actions().mouseMove(deleteButton).click().perform();
            browser.wait(EC.presenceOf($('.md-dialog-content')), 500);
            browser.actions().mouseMove({x: 10, y: 10}).click().perform();
            browser.wait(EC.stalenessOf($('.md-dialog-content')), 500);


        });
        it("check if delete reduce elements", function () {


        });
        it("check if moving to page for add new works", function () {

        });
        it("check if moving back to main table works", function () {

        });
        it("check if moving to edit works", function () {

        });

    });
});
