//jshint strict: false
exports.config = {

  allScriptsTimeout: 11000,

  specs: [
    '*.js'
  ],

  capabilities: {
    browserName: 'chrome'
  },
    onPrepare:function () {
        browser.addMockModule('core.scrumRetroRestService', function () {
            angular.module('core.scrumRetroRestService', []).service("scrumRetroRestService", ['$q', function ($q) {
                var mockActivities = [{
                    "id": 1,
                    "title": "Test",
                    "subtitle": "Test",
                    "category": "Set the stage",
                    "description": "Test",
                    "image": {
                        "filetype": "image/jpeg",
                        "filename": "test.jpg",
                        "filesize": 1199,
                        "base64": "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mNk+M9QDwADhgGAWjR9awAAAABJRU5ErkJggg=="
                    }
                },
                    {
                    "id": 2,
                    "title": "Test3",
                    "subtitle": "Test3",
                    "category": "Set the stage",
                    "description": "Test secundo",
                    "image": {
                        "filetype": "image/jpeg",
                        "filename": "test2.jpg",
                        "filesize": 1199,
                        "base64": "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII="
                    }
                }, {
                    "id": 3,
                    "title": "Test2",
                    "subtitle": "Test2",
                    "category": "Gather data",
                    "description": "Test2"
                }

                ];
                this.getAllRecords = function () {
                    return $q.when(mockActivities);
                };
                this.getOneRecord = function (id) {
                    var record =mockActivities.filter(function (id) {
                        return value.id===id;
                    })[0];

                    return $q.when(record);
                };
                this.addRecordToDb = function (data) {
                    return $q.when(mockActivities.push(data));
                };
                this.updateRecord = function (data) {
                    var record=mockActivities.filter(function (value) {
                        return value.id===data.id;
                    })[0];
                    var index=mockActivities.indexOf(record);
                    mockActivities[index]=data;
                    return $q;
                };
                this.deleteRecord = function (id) {
                    var record=mockActivities.filter(function (value) {
                        return value.id===id;
                    })[0];
                    var index=mockActivities.indexOf(record);
                    mockActivities.splice(index,1)
                    return $q;

                };


                this.getRecordsByCategory=function (category) {
                    var records=mockActivities.filter(function (value) {
                        return value.category==category;
                    });
                    return $q.when(records);
                };



            }])
        });

    },

  baseUrl: 'http://localhost:8000/',

  framework: 'jasmine',

  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }

};
