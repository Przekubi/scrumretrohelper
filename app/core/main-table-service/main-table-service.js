angular.module("core.mainTableService", []).factory('mainTableService', [function () {
    var mainTable = {}
    mainTable.visibility = true;
    mainTable.SetVisibility = function (value) {
        mainTable.visibility = value;
    }
    return mainTable;
}]);