//TODO: Ogarnąć jak ułożyć poprawny serwis
angular.module('core.scrumRetroRestService', ['ngResource']).service("scrumRetroRestService", ['$resource', function ($resource) {
    const httpAdressOfScrumRetroTable = "http://localhost:3000/scrumRetro/:id";
    this.getAllRecords = function () {
        var records = $resource(httpAdressOfScrumRetroTable);
        return records.query().$promise;

    };
    this.getOneRecord = function (id) {
        var record = $resource(httpAdressOfScrumRetroTable, {},
            {
                get: {
                    method: 'GET',
                    params: {id: id}
                }
            });
        return record.get().$promise;
    };
    this.addRecordToDb = function (data) {
        var record = $resource(httpAdressOfScrumRetroTable, {
                description: data.description,
                category: data.category
            },
            {
                save: {
                    method: 'POST',
                }
            });
        return record.save(data).$promise;
    };
    this.updateRecord = function (data) {
        var record = $resource(httpAdressOfScrumRetroTable, {},
            {
                update: {
                    method: 'PUT',
                    params: {id: data.id}
                }
            });
        return record.update(data).$promise;
    };
    this.deleteRecord = function (id) {
        var record = $resource(httpAdressOfScrumRetroTable, {},
            {
                delete: {
                    method: 'DELETE',
                    params: {id: id}
                }
            });
        return record.delete().$promise;

    };


    this.getRecordsByCategory = function (category) {
        var records = $resource(httpAdressOfScrumRetroTable, {},
            {
                query: {
                    method: 'GET',
                    params: {category: category},
                    isArray: true
                }
            });
        return records.query().$promise;


    };

}]);