angular.module("core.scrumArrayRandomDrawService", []).service("scrumArrayRandomDrawService", [function () {
    var randomDraw = function (data) {
        return data[Math.floor(Math.random() * data.length)];

    }
    this.randomDrawRetroActivity = function randomDrawRetroActivity(data, lastId) {
        switch (data.length) {
            case(0):
                return {description: "Brak elementów w bazie danych"};
            case(1):
                return data[0];
            default:
                var randomItem = data[Math.floor(Math.random() * data.length)];
                if (randomItem.id === lastId) {
                    return randomDrawRetroActivity(data, lastId);
                }
                return randomItem;

        }
    };
    this.randomDrawArrayOfNumbersDifferentFromPreviousFromZeroToFour = function (lengthOfArray) {
        var data = [0, 1, 2, 3, 4];
        var newDataArray = [];
        var i = 0;
        while (i !== lengthOfArray) {
            var randomItem = randomDraw(data);
            if (newDataArray.length == 0) {
                newDataArray.push(randomItem);
            } else if (newDataArray[i - 1] == randomItem) {
                continue;
            } else {
                newDataArray.push(randomItem);
            }
            i++
        }
        return newDataArray;
    };
}]);