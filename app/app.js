'use strict';

// Declare app level module which depends on views, and core components
angular.module('myApp', [
    'ngRoute',
    'ngMaterial',
    'angularFileUpload',
    'core.scrumRetroRestService',
    'core.scrumArrayRandomDrawService',
    'core.mainTableService',
    'scrumRetroTable',
    'scrumAddEdit',
    'scrumSearch',
    'scrumCategorySearch',
    'bootstrapLightbox',
    'retroActivityDirective'
])
    .config(['$locationProvider', '$routeProvider', function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider
            .when('/scrumRetroTable', {template: '<scrum-retro-table></scrum-retro-table>'})
            .when('/scrumAdd/:category', {template: '<scrum-add-edit></scrum-add-edit>'})
            .when('/scrumEdit/:id/:category', {template: '<scrum-add-edit></scrum-add-edit>'})
            .when('/scrumCategorySearch/:category', {template: '<scrum-category-search></scrum-category-search>'})
            .when('/scrumSearch', {template: '<scrum-search></scrum-search>'})
            .otherwise('/scrumRetroTable');
    }]);
