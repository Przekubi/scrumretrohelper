angular.module("scrumSearch", ['core.scrumRetroRestService', 'core.scrumArrayRandomDrawService']).component('scrumSearch', {
    templateUrl: "scrum-search/scrum-search.html",
    controller: ['scrumRetroRestService', 'scrumArrayRandomDrawService', "Lightbox", function ScrumSearchCtrl(scrumRetroRestService, scrumArrayRandomDrawService, Lightbox) {
        var me = this;
        scrumRetroRestService.getAllRecords().then(function (data) {
            me.allRetroActivities = data;
            me.backgroundClassNumberArray = scrumArrayRandomDrawService.randomDrawArrayOfNumbersDifferentFromPreviousFromZeroToFour(data.length);

        });
        var recordFilterQuery = function (item) {
            var me = this;
            if (me.searchQuery !== undefined && me.searchQuery !== "") {
                if (item.hasOwnProperty("title")) {
                    if (item.title.toLowerCase().includes(me.searchQuery.toLowerCase())) {
                        return true;
                    }
                }
                if (item.hasOwnProperty("subtitle")) {
                    if (item.subtitle.toLowerCase().includes(me.searchQuery.toLowerCase())) {
                        return true;
                    }
                }
                if (item.hasOwnProperty("description")) {
                    if (item.description.toLowerCase().includes(me.searchQuery.toLowerCase())) {
                        return true;
                    }
                }
            }
            return false;

        };
        me.search = recordFilterQuery.bind(me);
        me.openLighboxModal = function (index) {
            var images = [];
            var records = me.allRetroActivities.filter(recordFilterQuery.bind(me));

            angular.forEach(records, function (value, key) {
                if (value.image) {
                    images[key] = ("data:image/*;base64," + value.image.base64);
                }
            });
            Lightbox.openModal(images, index);
        };

    }]
});