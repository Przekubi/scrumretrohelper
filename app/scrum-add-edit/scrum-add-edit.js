angular.module('scrumAddEdit', ['ngRoute', 'core.scrumRetroRestService']).component('scrumAddEdit', {
    templateUrl: 'scrum-add-edit/scrum-add-edit.html',
    controller: ['scrumRetroRestService', '$routeParams', "$window", function ScrumAddEditCtrl(scrumRetroRestService, $routeParams, $window) {
        var me = this;
        me.recordId = $routeParams.id;
        var redirectToCategorySearch = function () {
            $window.location.href = "/#!/scrumCategorySearch/"+$routeParams.category;
        };
        var add = function (valid) {
            var me = this;
            if (!valid) {
                return;
            }
            var data = {
                title:me.title,
                subtitle:me.subtitle,
                category: me.category,
                description: me.description,
                image: me.image
            };
            scrumRetroRestService.addRecordToDb(data).then(redirectToCategorySearch);
        };
        var update = function (valid) {
            var me = this;
            if (!valid) {
                return;
            }
            var data = {
                id: me.recordId,
                title:me.title,
                subtitle:me.subtitle,
                category: me.category,
                description: me.description,
                image: me.image
            };
            scrumRetroRestService.updateRecord(data).then(redirectToCategorySearch);
        };
        me.categories = ["Set the stage", "Gather data", "Generate insights", "Decide what to do", "Close the meeting"];
        if (me.recordId === undefined) {
            me.category = $routeParams.category;
            me.submitForm = add.bind(me);
        } else {
            scrumRetroRestService.getOneRecord(me.recordId).then(function (data) {
                me.title=data.title;
                me.subtitle=data.subtitle;
                me.category = data.category;
                me.description = data.description;
                me.image = data.image;
            });
            me.submitForm = update.bind(me);
        }


    }]
});