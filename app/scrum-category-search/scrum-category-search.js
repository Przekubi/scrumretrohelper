angular.module("scrumCategorySearch", ['ngRoute', 'ngMaterial', 'core.scrumRetroRestService', 'core.scrumArrayRandomDrawService']).component('scrumCategorySearch', {
    templateUrl: 'scrum-category-search/scrum-category-search.html',
    controller: ['scrumRetroRestService', 'scrumArrayRandomDrawService', 'Lightbox', '$routeParams', '$mdDialog', '$window',
        function ScrumCategorySearchCtrl(scrumRetroRestService, scrumArrayRandomDrawService, Lightbox, $routeParams, $mdDialog, $window) {
            var me = this;
            me.category = $routeParams.category;
            var assignDataToTable = function (data) {
                var me = this;
                me.retroCategoryActivities = data;
                me.backgroundClassNumberArray = scrumArrayRandomDrawService.randomDrawArrayOfNumbersDifferentFromPreviousFromZeroToFour(data.length);
            };
            scrumRetroRestService.getRecordsByCategory(me.category).then(assignDataToTable.bind(me));
            me.goToEdit = function (id, category) {
                $window.location.href = "#!/scrumEdit/" + id + "/" + category;
            };
            me.showDeleteConfirmation = function (id, ev) {
                var confirm = $mdDialog.confirm()
                    .title("Czy na pewno chcesz usunąć ten pomysł?")
                    .ariaLabel("Usunięcie")
                    .targetEvent(ev)
                    .clickOutsideToClose(true)
                    .ok("Tak")
                    .cancel("Nie");
                $mdDialog.show(confirm).then(
                    function () {
                        scrumRetroRestService.deleteRecord(id).then($window.location.reload());
                    }
                    ,
                    function () {
                    }
                );
            };
            me.openLighboxModal = function (index) {
                var images = [];
                angular.forEach(me.retroCategoryActivities, function (value, key) {
                    if (value.image) {
                        images[key] = ("data:image/*;base64," + value.image.base64);
                    }
                });
                Lightbox.openModal(images, index);
            };

        }]
});