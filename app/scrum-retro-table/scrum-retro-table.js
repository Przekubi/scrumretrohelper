'use strict';

angular.module('scrumRetroTable',
    ['ngRoute', 'core.scrumRetroRestService', 'core.scrumArrayRandomDrawService', 'core.mainTableService', 'angularFileUpload', 'naif.base64'])
    .component(
        'scrumRetroTable', {
            templateUrl: 'scrum-retro-table/scrum-retro-table.html',
            controller: ['scrumRetroRestService', 'scrumArrayRandomDrawService', 'mainTableService', 'FileUploader','Lightbox',
                function ScrumRetroTableCtrl(scrumRetroRestService, scrumArrayRandomDrawService, mainTableService, FileUploader,Lightbox) {
                    var me = this;
                    me.categories = ["Set the stage", "Gather data", "Generate insights", "Decide what to do", "Close the meeting"];
                    me.arrayOfRetroActivities = [];
                    me.arrayOfUploadImages=[];
                    me.inputSwitches=[];
                    Lightbox.keyboardNavEnabled=false;
                    me.backgroundClassNumberArray=scrumArrayRandomDrawService.randomDrawArrayOfNumbersDifferentFromPreviousFromZeroToFour(me.categories.length);
                    var assingRecordDataToTable = function (data) {
                        var me = this;
                        me.allRetroActivities = data;
                        for (var i = 0; i < me.categories.length; i++) {
                            var array = data.filter(function (value) {
                                return value.category === me.categories[i];
                            });
                            if (array !== 0) {
                                var dataToAssign;
                                if (me.arrayOfRetroActivities[i] === undefined) {
                                    dataToAssign = scrumArrayRandomDrawService.randomDrawRetroActivity(array);
                                }
                                else {
                                    dataToAssign = scrumArrayRandomDrawService.randomDrawRetroActivity(array, me.arrayOfRetroActivities[i].id);
                                }
                                me.arrayOfRetroActivities[i] = {
                                    id: dataToAssign.id,
                                    title: dataToAssign.title,
                                    subtitle:dataToAssign.subtitle,
                                    category: me.categories[i],
                                    description: dataToAssign.description,
                                    image: dataToAssign.image
                                };

                            }
                        }

                    };




                    me.openLighboxModal = function (index) {
                        var images=[];
                        angular.forEach(me.arrayOfRetroActivities, function (value, key) {
                            if(value.image) {
                                images[key] = ("data:image/*;base64," + value.image.base64);
                            }
                        });
                        Lightbox.openModal(images, index);
                    };
                    me.upload = function () {
                        for(var i=0; i<me.arrayOfRetroActivities.length; i++){
                            var item=me.arrayOfRetroActivities[i];
                            var image=me.arrayOfUploadImages[i];
                            if(image!==undefined) {
                                scrumRetroRestService.sendImageInBase64ToDatabase(item, image).then(function () {
                                    image=undefined;
                                });
                            }

                            me.inputSwitches[i]=false;

                        }
                    };


                    var assingOneRecordDataToTable = function (index, id, data) {
                        var me = this;
                        var dataToAssign = scrumArrayRandomDrawService.randomDrawRetroActivity(data, id);
                        me.arrayOfRetroActivities[index] = {
                            id: dataToAssign.id,
                            title:dataToAssign.title,
                            subtitle:dataToAssign.subtitle,
                            category: me.categories[index],
                            description: dataToAssign.description,
                            image: dataToAssign.image
                        };

                    };
                    me.drawNew = function () {
                        scrumRetroRestService.getAllRecords().then(assingRecordDataToTable.bind(me));
                    };

                    me.deleteItem = function (index, category, id) {
                        scrumRetroRestService.deleteRecord(id).then(function () {
                            me.drawSingle(index, category, id);
                        });
                    };
                    me.drawSingle = function (index, category, id) {
                        scrumRetroRestService.getRecordsByCategory(category).then(assingOneRecordDataToTable.bind(me, index, id));
                    };

                    me.checkIfEmpty = function (description) {
                        return description === "Brak elementów w bazie danych";
                    };
                }]
        });
 

